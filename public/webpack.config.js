/*
      <script src="/node_modules/angular/angular.js"></script>
    <script src="/node_modules/angular-aria/angular-aria.js"></script>
    <script src="/node_modules/angular-animate/angular-animate.js"></script>
    <script src="/node_modules/angular-material/angular-material.js"></script>
*/
module.exports = {
    
    entry:{
       angular:'./node_modules/angular/angular',
       angulararia:'./node_modules/angular-aria/angular-aria',
       animate:'./node_modules/angular-animate/angular-animate',
       material:'./node_modules/angular-material/angular-material' 
    },
    output:{
        filename:'angular-bundle.js',
        library:'angular-pack'
    },
    watch:true
}