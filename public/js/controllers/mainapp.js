app.controller('MainCtrl', ['$scope', function ($scope) {
     $scope.selectedTab = 0;
    
    $scope.changeTab = function(nmb) {
      $scope.selectedTab=nmb;
        
    }
}]);

app.directive('myTabcontent',function () {
    return {
        templateUrl:function (ell,atr) {
            
            return '../Views/'+atr.type+'.html';
        }
    }
})