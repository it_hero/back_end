var app= angular.module('app',['ngMaterial']).config(function ($mdThemingProvider) {
    $mdThemingProvider.theme('altTheme')
    .primaryPalette('red')
    .accentPalette('orange')
    $mdThemingProvider.setDefaultTheme('altTheme');
})
