var LocalStrat = require('passport-local').Strategy;
var GoogleStrat = require('passport-google-oauth').OAuth2Strategy;
var User = require('../db/UserSchema');
var config = require('./config');
function getUserAccount(user) {
   var _user={};
   if(user){
       
    if(user['twitter']['token']){
       _user.token= user['twitter']['token'];
       _user.id=user['twitter']['id'];
       _user.displayName=user['twitter']['displayName'];
       _user.username=user['twitter']['username'];
        return _user;
    }
    if(user['google']['token']){
        _user={
              id: user['google']['id'],
        token: user['google']['token'],
        email: user['google']['email'],
        name: user['google']['name']
        }
        return _user;
    }
    if(user['facebook']['token']){
        _user={
             id: user['facebook']['id'],
        token: user['facebook']['token'],
        email: user['facebook']['email'],
        name: user['facebook']['name']
        }
        return _user;
    }
    if(user['local']['email']){
        _user={
              email: user['local']['email'],
        name: user['local']['name'],
        nickname:user['local']['nickname'],
        birthday: user['local']['birthday'],
        expirience: user['local']['experience'],
        coins:user['local']['coins'],
        verified:user['local']['verified']
        }
        return _user;
    }
   }
}

module.exports = function (passport) {
    passport.serializeUser(function (user, done) {

        done(null, user.id);
    });
    passport.deserializeUser(function (id, done) {


        User.findById(id).exec(function (err, user) {
          if(user){
               done(null, getUserAccount(user));
          }
          else {
              done('error here');
          }

        });
    });
    //// GOOOGLE////
    passport.use(new GoogleStrat({
        clientID: config.google.gid,
        clientSecret: config.google.gsecret,
        callbackURL: config.google.redirect_uri
    }, function (token, refreshToken, profile, done) {
        process.nextTick(function () {
            User.findOne({ 'google.id': profile.id }, function (err, user) {
                if (err) return done(err);
                if (user) {
                    return done(null, user);
                } else {
                    var newUser = new User();
                    newUser.google.id = profile.id;
                    newUser.google.token = token,
                    newUser.google.name = profile.displayName;
                    newUser.email = profile.emails[0].value;
                    newUser.local.verified=true;
                    newUser.save(function (err) {
                        if (err) throw err;

                        return done(null, newUser);
                    });
                }
            });
        });
    }));
    
    
    
    
    
    /////////LOOOOOOOOOOOOCAAAAAAALLLLLLL    
    passport.use('local-login', new LocalStrat({
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true
    }, function (req, email, password, done) {
        User.findOne({ 'local.email': email }, function (err, user) {
            if (err) return done(err);
            if (!user) {
                return done(null, false, req.flash('loginMessage', 'User not found!'));
            } else {
                if (!user.validPassword(password)) {
                    return done(null, false, req.flash('loginMessage', 'Incorrect password!'));
                } else {

                    return done(null, user);
                }

            }
        });
    }));







    passport.use('local-signup', new LocalStrat({
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true
    }, function (req, email, password, done) {
        process.nextTick(function () {
            User.findOne({ 'local.email': email }, function (err, user) {
                if (user) {
                    return done(null, false, req.flash('signupMessage', 'Email already in use'));
                } else {
                    var newUser = new User();
                    newUser.email = email;
                    newUser.local.password = newUser.generateHash(password);
                    if (req.body.nickname) newUser.local.nickname = req.body.nickname
                    if (req.body.FirstName) newUser.local.name.push(req.body.FirstName)
                    if (req.body.Patronimic) newUser.local.name.push(req.body.Patronimic)
                    if (req.body.LastName) newUser.local.name.push(req.body.LastName)
                    if (req.body.bdate) newUser.local.birthday = new Date(req.body.bdate);

                    newUser.save(function (err) {
                        if (err) throw err;

                        return done(null, newUser);
                    });
                    User.findOne({ 'local.email': newUser.local.email }, function (err, user) {
                        if (err) return err;
                        var mail = require('./mailer');

                        mail(user.local.email, "http://localhost:3000/confirm/" + user.id);
                    })

                }
            })
        });
    }));
}