var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');
var UserSchema = mongoose.Schema({
     email: {
            type: String,
            unique: true
        },
       
    local: {
        name: [],
        password: String,
        nickname: String,
        birthday: Date,
        expirience: {
            type:String,
            default:0
        },
        coins: {
            type:Number,
            default:0
        },
        verified: {
            type: Boolean,
            default: false
        }
    },
    facebook: {
        id: String,
        token: String,
        name: String
    },
    twitter: {
        id: String,
        token: String,
        displayName: String,
        username: String
    },
    google: {
        id: String,
        token: String,
        
        name: String
    }
});


UserSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};
UserSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.local.password);
};
module.exports = mongoose.model('User', UserSchema);