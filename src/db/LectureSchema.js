var mongoose = require ('mongoose');
var LectureSchema = mongoose.Schema({
	title:String,
	description:String,
	content:String,
	status: String{
		enum:['published','not published','draft']
	},
	});
	module.exports = mongoose.model('Lecture',LectureSchema);