var mongoose = require ('mongoose');
var TaskSchema = mongoose.Schema({
	title: String,
	single:Boolean,
	price_task:Number,
	deadline:Date,
	level:String{
		enum:['easy','middle','hard']
	},
	});
module.exports = mongoose.model('Task',TaskSchema);


