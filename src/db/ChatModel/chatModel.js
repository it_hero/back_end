var mongoose=require('mongoose');
var ChatMessageSchema=mongoose.Schema({
    Sender:String,
    Content:String,
    ChatRoom:String,
    Date:Date
});
module.exports=mongoose.model('ChatMessage',ChatMessageSchema);
