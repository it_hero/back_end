module.exports = function(app, passport) {

	
	app.get('/', function(req, res,next) {
      
		res.render('index',{ title: 'Main Page' ,user:req.user}); 
	});

	
    app.get('/logout',function (req,res,next) {
        req.logout();
        res.redirect('/');
    })

	app.post('/login', passport.authenticate('local-login', {
		
        session:true
	}),function (req,res) {
      if(isLoggedIn){
          res.redirect('/');
      }
    });

	app.get('/confirm/:id',function (req,res) {
        var User= require('../src/db/UserSchema');
       
        User.findOne({_id:req.params.id},function (err,user) {
            if(err) res.json(err);
            if(user){
                if(!user.local.verified){
                    user.local.verified=true;
                    user.save(function (err) {
                         res.redirect('/');
                    })
                } else {
                    res.json({Verified:"User already verified"});
                }
            }
            else {
                res.json({error:"user not found"});
            }
        })
     });
     app.get('/auth/google/',passport.authenticate('google',{scope:['profile','email']}));
     app.get('/auth/callback',passport.authenticate('google',{
         successRedirect:'/',
         failureRedirect:'/',
         
        session:true
     }));
	

	app.post('/signup', passport.authenticate('local-signup', {
		successRedirect : '/', // redirect to the secure profile section
		failureRedirect : '/', // redirect back to the signup page if there is an error
		failureFlash : true // allow flash messages
        ,
        session:true
	}));


	app.get('/logout', function(req, res) {
		req.logout();
		res.redirect('/');
	});
};

// route middleware to make sure
function isLoggedIn(req, res, next) {

	// if user is authenticated in the session, carry on
	if (req.isAuthenticated())
		return next();

	// if they aren't redirect them to the home page
	res.redirect('/');
}